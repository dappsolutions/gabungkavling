package com.apps.gabungkavling.libraries

import com.android.volley.VolleyError

interface VolleyCallbackWithError{
    fun onSuccess(result:String)
    fun onError(error: VolleyError)
}
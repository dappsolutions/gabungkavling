package com.apps.gabungkavling.libraries

import android.view.MenuItem

interface NavCallback{
    fun getItemData(item:MenuItem)
}
package com.apps.gabungkavling.libraries

import com.android.volley.VolleyError

interface ErrorCallback{
    fun errorContaint(error: VolleyError)
}
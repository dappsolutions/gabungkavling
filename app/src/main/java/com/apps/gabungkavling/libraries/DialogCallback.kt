package com.apps.gabungkavling.libraries
import android.content.DialogInterface

interface DialogCallback{
    fun okAction(dialog: DialogInterface, i: Int)
}
package com.apps.gabungkavling.libraries

import android.view.View

interface ListCallback {
  fun onExecuteViewHolder(view:View, position:Int)
}
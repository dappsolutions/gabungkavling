package com.apps.gabungkavling.libraries;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;

public class Session {
    FragmentActivity activity;
    SharedPreferences setSession;
    Context context;

    public Session(FragmentActivity activity) {
        this.activity = activity;
        setSession = this.activity.getSharedPreferences("session",
                Context.MODE_PRIVATE);
    }

    public Session(Context ctx) {
        this.context = ctx;
        setSession = this.context.getSharedPreferences("session",
                Context.MODE_PRIVATE);
    }

    public void setUserId(String playerName) {
        SharedPreferences.Editor data_ses = setSession.edit();
        data_ses.putString("user_id", playerName);
        data_ses.apply();
    }

    public void setDepartemen(String departemen) {
        SharedPreferences.Editor data_ses = setSession.edit();
        data_ses.putString("departemen", departemen);
        data_ses.apply();
    }

    public String getUserId() {
        return setSession.getString("user_id", "missing");
    }

    public String getDepartemen() {
        return setSession.getString("departemen", "missing");
    }

    public void clearSession() {
        setSession.edit().remove("user_id").commit();
        setSession.edit().remove("departemen").commit();
//        setSession.edit().remove("nama").commit();
//        setSession.edit().remove("foto").commit();
//        setSession.edit().remove("no_hp").commit();
//        setSession.edit().remove("hak_akses").commit();
//        setSession.edit().remove("url").commit();
//        setSession.edit().remove("kompi").commit();
//        setSession.edit().remove("taruna_nama_wali").commit();
    }

}

package com.apps.gabungkavling

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.apps.gabungkavling.libraries.Application
import com.apps.gabungkavling.modules.gabung_kavling.view.GabungKavling
import com.apps.gabungkavling.modules.login.view.Login

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(intent.extras != null){
            var qrcode = intent.getStringExtra("qrcode")
            var ref = intent.getStringExtra("ref")
            var no_pkbb = intent.getStringExtra("no_pkbb")
            var bahan_baku = intent.getStringExtra("bahan_baku")
            var posisi = intent.getStringExtra("posisi")
            var kavling = intent.getStringExtra("kavling")

            val bundle = Bundle()
            bundle.putString("id", ref)
            bundle.putString("no_pkbb", no_pkbb)
            bundle.putString("bahan_baku", bahan_baku)
            bundle.putString("qrcode", qrcode)
            bundle.putString("posisi", posisi)
            bundle.putString("kavling", kavling)

            val gabungKavling = GabungKavling()
            gabungKavling.arguments = bundle

            Application().moveFragment(this, R.id.frameContent, gabungKavling)
        }else{
            Application().moveFragment(this, R.id.frameContent, Login())
        }
    }
}

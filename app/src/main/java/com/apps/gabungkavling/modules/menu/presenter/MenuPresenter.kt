package com.apps.gabungkavling.modules.menu.presenter

import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.apps.gabungkavling.R
import com.apps.gabungkavling.libraries.Application
import com.apps.gabungkavling.libraries.Helper
import com.apps.gabungkavling.libraries.ListCallback
import com.apps.gabungkavling.libraries.RvAdapter
import com.apps.gabungkavling.modules.menu.model.MenuModel
import com.apps.gabungkavling.modules.menu.view.Menu
import com.apps.gabungkavling.modules.pkbb.view.Pkbb
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.menu_utama_view.*
import org.jetbrains.anko.support.v4.toast

class MenuPresenter(val main:Menu){
    lateinit var listMenu : ArrayList<MenuModel>
    lateinit var adapter: RvAdapter

    init {
        setDataMenu()
        setlistDataMenu()
    }

    fun setDataMenu() {
        listMenu = ArrayList()
        listMenu.add(MenuModel("1", R.drawable.ic_kavling, "PENGGABUNGAN KAVLING"))
    }

    fun setlistDataMenu() {
        adapter = RvAdapter(listMenu as ArrayList<Any?>, object: ListCallback {
            override fun onExecuteViewHolder(view: View, position: Int) {
                val contentMenu = view.findViewById(R.id.contentMenu) as LinearLayout
                val txtTitle = view.findViewById(R.id.txtTitleMenu) as TextView
                val imgIcon = view.findViewById(R.id.imgIcon) as CircleImageView

                txtTitle.text = listMenu[position].keterangan
                imgIcon.setImageResource(listMenu[position].img)
                contentMenu.setOnClickListener {
                    main.toast(listMenu[position].keterangan)

                    when(listMenu[position].idCard){
                        "1" ->{
                            Application().moveFragment(main.activity, R.id.frameContent,
                                Pkbb()
                            )
                        }
                    }
                }
            }
        }, main.activity!!, R.layout.list_menu)

        Helper(main.activity, main.rvMenu, adapter).setRecycleviewGrid(3)
    }


}
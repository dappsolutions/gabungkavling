package com.apps.gabungkavling.modules.scan.presenter

import android.util.Log
import android.view.View
import com.android.volley.VolleyError
import com.apps.gabungkavling.libraries.ApiService
import com.apps.gabungkavling.libraries.Error
import com.apps.gabungkavling.libraries.Message
import com.apps.gabungkavling.libraries.Network
import com.apps.gabungkavling.libraries.VolleyCallbackWithError
import com.apps.gabungkavling.modules.scan.view.ScanActivity
import com.google.android.gms.vision.barcode.Barcode
import kotlinx.android.synthetic.main.gabung_kavling_view.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject

class ScanPresenter(val main:ScanActivity){
    var id_ref:String
    var baseUrl = "http://10.100.2.70/pobb"
    init {
        id_ref = main.intent.getStringExtra("ref")
    }

    fun prosesVerifikasi(barcode: Barcode?) {
        val url = ApiService.URL.baseUrl(baseUrl,"reservasi_silo", "verifikasi")
        val params = HashMap<String, String>()
        params["ref"] = id_ref
        params["qrcode"] = barcode?.displayValue.toString()

        val msg = Message(main)
        msg.showMessage("Proses Verifikasi Data...")

        doAsync {
            Network(main.applicationContext).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("Verifikasi", result)
                    if(JSONObject(result).get("is_valid").toString().equals("true")){
                        msg.showMessage("Proses Verifikasi Berhasil")

                        main.finish()
                    }
                }

                override fun onError(error: VolleyError) {
                    val msge = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    msg.showMessage(msge)
                }
            })
        }
    }
}
package com.apps.gabungkavling.modules.gabung_kavling.model

import com.google.gson.annotations.SerializedName

data class Kavling(
    @SerializedName("no_pkbb")
    var no_pkbb:String,
    @SerializedName("kavling")
    var kavling:String,
    @SerializedName("no_op")
    var no_op:String,
    @SerializedName("tonase")
    var tonase:String,
    @SerializedName("lot_history")
    var lot_history:String,
    @SerializedName("verified")
    var verified:String
//    @SerializedName("verified_2")
//    var verified_2:String,
//    @SerializedName("verified_3")
//    var verified_3:String
)

data class KavlingResponse(val data : List<Kavling>)
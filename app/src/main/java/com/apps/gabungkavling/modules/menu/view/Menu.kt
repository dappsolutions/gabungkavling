package com.apps.gabungkavling.modules.menu.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.gabungkavling.R
import com.apps.gabungkavling.libraries.Application
import com.apps.gabungkavling.libraries.Session
import com.apps.gabungkavling.modules.menu.presenter.MenuPresenter
import kotlinx.android.synthetic.main.menu_utama_view.*

class Menu : Fragment(){
    lateinit var presenter:MenuPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.menu_utama_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = MenuPresenter(this)

        btnLogout.setOnClickListener {
            Session(context).clearSession()
            Application().firstActivity(context)
        }
    }
}
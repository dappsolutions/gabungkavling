package com.apps.gabungkavling.modules.scan.view

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.SparseArray
import com.google.android.gms.vision.barcode.Barcode
import info.androidhive.barcode.BarcodeReader
import android.util.Log
import com.apps.gabungkavling.MainActivity
import com.apps.gabungkavling.R
import com.apps.gabungkavling.libraries.Message
import com.apps.gabungkavling.modules.scan.presenter.ScanPresenter


class ScanActivity : AppCompatActivity(), BarcodeReader.BarcodeReaderListener {

    lateinit var barcodeReader: BarcodeReader
    lateinit var presenter:ScanPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.scan_barcode_view)
//        presenter = ScanPresenter(this)

        Log.e("KAVLING", intent.getStringExtra("kavling"))
        barcodeReader = supportFragmentManager.findFragmentById(R.id.barcode_scanner) as BarcodeReader
    }

    override fun onBitmapScanned(sparseArray: SparseArray<Barcode>?) {
        Log.e("Bitmap Sacan", "Scanened")
    }

    override fun onScannedMultiple(barcodes: MutableList<Barcode>?) {
        Log.e("Multiple", "Scanened")
    }

    override fun onCameraPermissionDenied() {
        Log.e("Permission", "Camera Denied")
    }

    override fun onScanned(barcode: Barcode?) {
        // playing barcode reader beep sound
        barcodeReader.playBeep()

        Log.e("Barcode", barcode?.displayValue)

//        presenter.prosesVerifikasi(barcode)
        // ticket details activity by passing barcode
        val barcodeAct = Intent(this@ScanActivity, MainActivity::class.java)
        barcodeAct.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        barcodeAct.putExtra("qrcode", barcode?.displayValue)
        barcodeAct.putExtra("ref", intent.getStringExtra("ref"))
        barcodeAct.putExtra("no_pkbb", intent.getStringExtra("no_pkbb"))
        barcodeAct.putExtra("bahan_baku", intent.getStringExtra("bahan_baku"))
        barcodeAct.putExtra("kavling", intent.getStringExtra("kavling"))
        barcodeAct.putExtra("posisi", intent.getStringExtra("posisi"))
        startActivity(barcodeAct)
    }

    override fun onScanError(errorMessage: String?) {
        Message(this).showMessage("Gagal Scan")
    }
}

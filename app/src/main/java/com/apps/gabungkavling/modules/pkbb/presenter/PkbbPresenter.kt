package com.apps.gabungkavling.modules.pkbb.presenter

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.gabungkavling.R
import com.apps.gabungkavling.libraries.*
import com.apps.gabungkavling.modules.gabung_kavling.view.GabungKavling
import com.apps.gabungkavling.modules.pkbb.model.PkbbModel
import com.apps.gabungkavling.modules.pkbb.model.PkbbModelResponse
import com.apps.gabungkavling.modules.pkbb.view.Pkbb
import com.google.gson.Gson
import kotlinx.android.synthetic.main.pkbb_view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.doAsync

class PkbbPresenter (val main: Pkbb){
    lateinit var adapter:RvAdapter
    var baseUrl = "http://10.100.2.70/pobb"
    init {
        getLisDataPkbb()
    }

    fun getLisDataPkbb() {
        val url = ApiService.URL.baseUrl(baseUrl,"reservasi_silo", "getListPkbb")
        val params = HashMap<String, String>()

        val msg = Message(main.context)
        msg.showLoading("Proses Retrieving Data...")

        try{
            GlobalScope.launch (Dispatchers.Main){
                Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                    override fun onSuccess(result: String) {
                        Log.e("DataPkbb", result)
                        msg.hideLoading()

                        val data = Gson().fromJson(result, PkbbModelResponse::class.java)
                        if(data.data.size > 0){
                            setlistDataPkbb(data.data)
                        }
                    }

                    override fun onError(error: VolleyError) {
                        msg.hideLoading()
                        val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                        Message(main.context).showMessage(msg)
                    }
                })
            }
        }catch (ex:Exception){
            Log.e("Error", ex.message.toString())
        }
    }

    fun setlistDataPkbb(data: List<PkbbModel>) {
        adapter = RvAdapter(data as ArrayList<Any?>, object: ListCallback {
            override fun onExecuteViewHolder(view: View, position: Int) {
                val txtPkbb = view.findViewById(R.id.txtPkbb) as TextView
                val contentPkbb = view.findViewById(R.id.contentPkbb) as LinearLayout
                txtPkbb.text = data[position].no_pkbb

                contentPkbb.setOnClickListener {
                    val bundle = Bundle()
                    bundle.putString("id", data[position].id)
                    bundle.putString("no_pkbb", data[position].no_pkbb)
                    bundle.putString("bahan_baku", data[position].bahan_baku)
                    val gabungKavling = GabungKavling()
                    gabungKavling.arguments = bundle

                    Application().moveFragment(main.activity, R.id.frameContent, gabungKavling)
                }
            }
        }, main.activity!!, R.layout.list_pkbb)

        Helper(main.activity, main.rvListPkbb, adapter).setRecycleview()
    }
}
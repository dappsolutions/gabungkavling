package com.apps.gabungkavling.modules.gabung_kavling.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.gabungkavling.R
import com.apps.gabungkavling.modules.gabung_kavling.presenter.GabungKavlingPresenter
import kotlinx.android.synthetic.main.gabung_kavling_view.*

class GabungKavling : Fragment(){
    lateinit var presenter:GabungKavlingPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.gabung_kavling_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = GabungKavlingPresenter(this)

        cardVerivikasi.setOnClickListener {
            presenter.prosesVerifikasi()
        }
    }
}
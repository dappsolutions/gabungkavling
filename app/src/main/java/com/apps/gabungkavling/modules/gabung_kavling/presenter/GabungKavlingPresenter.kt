package com.apps.gabungkavling.modules.gabung_kavling.presenter

import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.gabungkavling.R
import com.apps.gabungkavling.libraries.*
import com.apps.gabungkavling.modules.gabung_kavling.model.Kavling
import com.apps.gabungkavling.modules.gabung_kavling.model.KavlingResponse
import com.apps.gabungkavling.modules.gabung_kavling.view.GabungKavling
import com.apps.gabungkavling.modules.login.view.Login
import com.apps.gabungkavling.modules.scan.view.ScanActivity
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.gabung_kavling_view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.support.v4.startActivity
import org.json.JSONObject



class GabungKavlingPresenter(val main: GabungKavling) {
    lateinit var no_pkbb: String
    lateinit var bahan_baku: String
    lateinit var id_ref: String
    lateinit var qrcode: String
    lateinit var kavling: String

    lateinit var adapter: RvAdapter
    var baseUrl = "http://10.100.2.70/pobb"

    lateinit var last_kavling: String
    var siap_verifikasi = "0"

    val msg: Message

    init {
        msg = Message(main.context)
        if (main.arguments!!.getString("id") != null) {
            id_ref = main.arguments!!.getString("id")
            no_pkbb = main.arguments!!.getString("no_pkbb")
            bahan_baku = main.arguments!!.getString("bahan_baku")
            qrcode = main.arguments?.getString("qrcode").toString()
            kavling = main.arguments?.getString("kavling").toString()

            setInitData()
        } else {
            Application().moveFragment(main.activity, R.id.frameContent, Login())
        }
    }

    fun setInitData() {
        var nama_bb = bahan_baku.trim().replace("RM_", "")
        main.txtBahanBaku.text = "BAHAN BAKU : $nama_bb"

        getListKavling()
    }

    fun getKavlingSiapVerivikasi(){
        val url = ApiService.URL.baseUrl(baseUrl, "reservasi_silo", "kavlingSiapVerifikasi")
        val params = HashMap<String, String>()
        params["no_pkbb"] = no_pkbb
        params["user"] = Session(main.context).userId

        val msg = Message(main.context)
        msg.showLoading("Proses Retrieving Data...")
        try{
            GlobalScope.launch(Dispatchers.Main) {
                Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                    override fun onSuccess(result: String) {
                        Log.e("SiapVerifikasi", result)
                        msg.hideLoading()

                        siap_verifikasi = result

                        if(siap_verifikasi.equals("1")){
                            main.cardVerivikasi.visibility = VISIBLE
                        }
                    }

                    override fun onError(error: VolleyError) {
                        msg.hideLoading()
                        val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                        Message(main.context).showMessage(msg)
                    }
                })
            }
        }catch (ex:Exception){
            Log.e("ErrorVerifikasi", ex.message.toString())
        }
    }

    fun getListKavling() {
        val url = ApiService.URL.baseUrl(baseUrl, "reservasi_silo", "getDetailPkbb")
        val params = HashMap<String, String>()
        params["no_pkbb"] = no_pkbb
        params["bahan_baku"] = bahan_baku
        params["user"] = Session(main.context).userId

        val msg = Message(main.context)
        msg.showLoading("Proses Retrieving Data...")
        GlobalScope.launch(Dispatchers.Main) {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    msg.hideLoading()

                    val data = Gson().fromJson(result, KavlingResponse::class.java)
                    if (qrcode != "null") {
                        Log.e("NOPKBB-QRC", no_pkbb)
                        val kavling = main.arguments?.getString("kavling").toString().trim()
                        Log.e("KAVLING", kavling)
//                        if(kavling == kavling){
                        Log.e("NOPKBB", no_pkbb)
                        main.txtNoKavlingInfo.text = no_pkbb
//                        }
                    }
                    setlistDetailPkbb(data.data)
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main.context).showMessage(msg)
                }
            })
        }
    }

    fun setlistDetailPkbb(data: List<Kavling>) {
        try {
            last_kavling = data[data.size - 1].kavling

            var departemen = Session(main.context).departemen

            Log.e("DATa RESULT", data.toString());
            adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback {
                override fun onExecuteViewHolder(view: View, position: Int) {
                    Log.e("Error DAta ", data[position].toString());




                    val txtKavling = view.findViewById(R.id.txtKavling) as TextView
                    val txtRib = view.findViewById(R.id.txtRib) as TextView
                    val txtTonase = view.findViewById(R.id.txtTonase) as TextView
                    val txtScan = view.findViewById(R.id.txtScan) as TextView

                    val imgScan = view.findViewById(R.id.imgScan) as CircleImageView

                    txtKavling.text = data[position].kavling.toString()
                    txtRib.text = data[position].no_op.toString()
                    txtTonase.text = data[position].tonase.toString()
                    val kavling_list = data[position].kavling.toString()

//                    if(kavling_list == "J 6-7"){
                        Log.e("KAVLING J 6-7", kavling_list+" status verifide "+data[position].verified);
//                    }
//                    Log.e("KAVLING ", kavling_list+" status verifide "+data[position].verified);
                    if (kavling_list == kavling) {
                        //update to lot_join
                        imgScan.visibility = GONE
                        txtScan.text = "Scanned"
                        updateScannedToLotJoin(data[position])
                    }

//                    Log.e("KAVLING SESSION", kavling);


//                    if(departemen.equals("gudang") ||
//                        departemen.equals("gudangbb")){
                        if(data[position].verified.equals("1")){
                            imgScan.visibility = GONE
                            txtScan.text = "Scanned"
                        }else{
                            imgScan.visibility = VISIBLE
                            txtScan.text = ""
                        }
//                    }
//
//                    if(departemen.equals("qc-bb") ||
//                        departemen.equals("qc") ||
//                        departemen.equals("qc-proses")){
//                        if(data[position].verified.equals("1") &&
//                            data[position].verified_2.equals("1")){
//                            imgScan.visibility = GONE
//                            txtScan.text = "Scanned"
//                        }
//                    }
//
//                    if(departemen.equals("pengawasan")){
//                        if(data[position].verified.equals("1")
//                            && data[position].verified_2.equals("1")
//                            && data[position].verified_3.equals("1")){
//                            imgScan.visibility = GONE
//                            txtScan.text = "Scanned"
//                        }
//                    }



                    imgScan.setOnClickListener {
                        //                    Log.e("KAVLING", kavling)
                        main.startActivity<ScanActivity>(
                            "ref" to id_ref, "no_pkbb" to no_pkbb,
                            "bahan_baku" to bahan_baku, "kavling" to kavling_list,
                            "posisi" to position
                        )
                    }
                }
            }, main.activity!!, R.layout.list_kavling)

            Helper(main.activity, main.rvListKavling, adapter).setRecycleview()
            setProsesVerifikasi()
        } catch (ex: Exception) {
            Log.e("Error", ex.message.toString())
        }
    }

    private fun updateScannedToLotJoin(kavling: Kavling) {
        val params = HashMap<String, String>()
        params["lot_history"] = kavling.lot_history
        params["departemen"] = Session(main.context).departemen

        val url = ApiService.URL.baseUrl(baseUrl, "reservasi_silo", "updateLotJoin")
        msg.showLoading("Proses Updated data...")
        GlobalScope.launch(Dispatchers.Main) {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    msg.hideLoading()
                    msg.showMessage("Scanned Berhasil")
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val err = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    msg.showMessage(err)
                }
            })
        }

    }

    fun setProsesVerifikasi() {
        Log.e("QRCODE", qrcode)
        if (qrcode != "null") {
            Log.e("qrcodenotnull", qrcode)
            Log.e("Kavling", kavling)
            if (kavling == last_kavling) {
//                prosesVerifikasi()
            } else {
                Message(main.context).showMessage("Proses Scan Berhasil")
            }
        }

        getKavlingSiapVerivikasi()
    }


    fun prosesVerifikasi() {
        val url = ApiService.URL.baseUrl(baseUrl, "reservasi_silo", "verifikasi")
        val params = HashMap<String, String>()
        params["user"] = Session(main.context).userId
        params["ref"] = id_ref
        params["qrcode"] = qrcode
        params["departemen"] = Session(main.context).departemen

//        Log.e("Departemen", params.toString())
//        return
        val msg = Message(main.context)
        msg.showLoading("Proses Verifikasi Data...")
        GlobalScope.launch (Dispatchers.Main) {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("Verifikasi", result)
                    msg.hideLoading()
                    if (JSONObject(result).get("is_valid").toString().equals("true")) {
                        msg.showMessage("Proses Verifikasi Berhasil")
                        main.cardVerivikasi.visibility = GONE
                    } else {
                        msg.showMessage(JSONObject(result).get("message").toString())
                    }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main.context).showMessage(msg)
                }
            })
        }
    }
}
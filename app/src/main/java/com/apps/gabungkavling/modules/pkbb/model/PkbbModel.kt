package com.apps.gabungkavling.modules.pkbb.model

import com.google.gson.annotations.SerializedName

data class PkbbModel(
    @SerializedName("id")
    var id:String,
    @SerializedName("no_pkbb")
    var no_pkbb:String,
    @SerializedName("bahan_baku")
    var bahan_baku:String
)

data class PkbbModelResponse(val data:List<PkbbModel>)
package com.apps.gabungkavling.modules.login.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.gabungkavling.R
import com.apps.gabungkavling.libraries.Application
import com.apps.gabungkavling.libraries.Session
import com.apps.gabungkavling.modules.login.presenter.LoginPresenter
import com.apps.gabungkavling.modules.menu.view.Menu
import kotlinx.android.synthetic.main.login_view.*

class Login: Fragment(){
    lateinit var presenter:LoginPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.login_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = LoginPresenter(this)

        cardLogin.setOnClickListener {
            presenter.login()
        }
    }

    override fun onStart() {
        super.onStart()
        if(!Session(context).userId.equals("missing")){
            Application().moveFragment(activity, R.id.frameContent, Menu())
        }
    }
}
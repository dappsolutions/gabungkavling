package com.apps.gabungkavling.modules.login.presenter

import android.util.Log
import com.android.volley.VolleyError
import com.apps.gabungkavling.R
import com.apps.gabungkavling.libraries.*
import com.apps.gabungkavling.modules.login.view.Login
import com.apps.gabungkavling.modules.menu.view.Menu
import kotlinx.android.synthetic.main.login_view.*
import org.json.JSONObject


class LoginPresenter(val main: Login) {
    init {

    }

    fun login() {
        var baseUrl = "http://10.100.2.70/pobb"
        val url = ApiService.URL.baseUrl(baseUrl, "user", "loginApp")
        Log.e("URL", url)
        val params = HashMap<String, String>()
        params["user"] = main.edtUsername.text.toString()
        params["password"] = main.edtPassword.text.toString()

        Log.e("User", params["user"])
        Log.e("Password", params["password"])
        val msg = Message(main.context)
        msg.showLoading("Proses Login...")
        Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
            override fun onSuccess(result: String) {
                msg.hideLoading()
                Log.e("DataLogin", result)
                val jObj = JSONObject(result)
                if (jObj.get("status").toString().equals("1")) {
                    Session(main.context).userId = jObj.get("user_id").toString()
                    Session(main.context).departemen = jObj.get("departemen").toString()
                    Application().moveFragment(main.activity, R.id.frameContent, Menu())
                } else {
                    Message(main.context).showMessage("Login Gagal")
                }
            }

            override fun onError(error: VolleyError) {
                msg.hideLoading()
                Log.e("dataError", error.message.toString())
                val err = Error().checkOnErrorVolleyNetworkWithMessage(error)
                Message(main.context).showMessage(err)
            }
        })
    }
}
package com.apps.gabungkavling.modules.pkbb.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.gabungkavling.R
import com.apps.gabungkavling.modules.pkbb.presenter.PkbbPresenter

class Pkbb : Fragment(){
    lateinit var presenter: PkbbPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.pkbb_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = PkbbPresenter(this)
    }
}